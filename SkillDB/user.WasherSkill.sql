﻿CREATE TABLE [user].[WasherSkill]
(
	[Id] INT IDENTITY(1,1) NOT NULL , 
    [WasherId] INT NOT NULL, 
    [SkillId] INT NOT NULL, 
    CONSTRAINT [FK_WasherSkill_Washer] FOREIGN KEY (WasherId) REFERENCES [user].[Washer](Id), 
    CONSTRAINT [FK_WasherSkill_Skill] FOREIGN KEY (SkillId) REFERENCES [user].[Skill](Id), 
    PRIMARY KEY CLUSTERED ([WasherId], [SkillId])
)

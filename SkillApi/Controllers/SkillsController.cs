﻿using SkillApi.Models;
using SkillDataAccess;
using System.Linq;
using System.Web.Http;

namespace SkillApi.Controllers
{
    [RoutePrefix("api/skills")]
    public class SkillsController : ApiController
    {
        [Route("{skillId}")]
        [HttpGet]
        public IHttpActionResult GetSkill(int skillId)
        {
            if (skillId <= 0)
                return BadRequest("Skill id not valid");

            Skill skill = null;
            using (SkillDBEntities entities = new SkillDBEntities())
            {
                skill = entities.Skills.FirstOrDefault(x => x.Id == skillId);
            }

            if (skill == null)
            {
                return NotFound();
            }

            return Ok(new SkillViewModel() { Id = skill.Id, Name = skill.Name });
        }

        [Route("create")]
        [HttpPost]
        public IHttpActionResult CreateSkill([FromBody] CreateOrUpdateViewModel skill)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data");

            using (SkillDBEntities entities = new SkillDBEntities())
            {
                entities.Skills.Add(new Skill() { Name = skill.Name });
                entities.SaveChanges();
            }

            return Ok();
        }

        [Route("update/{skillId}")]
        [HttpPut]
        public IHttpActionResult UpdateSkill(int skillId, [FromBody] CreateOrUpdateViewModel skill)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data");

            using (SkillDBEntities entities = new SkillDBEntities())
            {
                var existingSkill = entities.Skills.FirstOrDefault(s => s.Id == skillId);

                if (existingSkill != null)
                {
                    existingSkill.Name = skill.Name;
                    entities.SaveChanges();
                }
                else
                {
                    return NotFound();
                }
            }

            return Ok();
        }

        [Route("delete/{skillId}")]
        [HttpDelete]
        public IHttpActionResult DeleteSkill(int skillId)
        {
            if (skillId <= 0)
                return BadRequest("Skill id not valid");

            using (SkillDBEntities entities = new SkillDBEntities())
            {
                var skillToDelete = entities.Skills.FirstOrDefault(x => x.Id == skillId);

                if (skillToDelete != null)
                {
                    if (skillToDelete.WasherSkills.Count() == 0)
                    {
                        entities.Skills.Remove(skillToDelete);
                        entities.SaveChanges();
                    }
                    else
                    {
                        return BadRequest("Skill is assigned!");
                    }
                }
                else
                {
                    return NotFound();
                }
            }

            return Ok();
        }

        [Route("assign/{skillId}/{washerId}")]
        public IHttpActionResult AssignSkill(int skillId, int washerId)
        {
            if (skillId <= 0 || washerId <= 0)
                return BadRequest("Skill/washer id not valid");

            using (SkillDBEntities entities = new SkillDBEntities())
            {
                var skill = entities.Skills.FirstOrDefault(x => x.Id == skillId);
                var washer = entities.Washers.FirstOrDefault(x => x.Id == washerId);
                if (skill != null && washer != null)
                {
                    if (skill.WasherSkills.Any(x => x.WasherId == washerId))
                    {
                        return BadRequest("Assignment already exist");
                    }

                    entities.WasherSkills.Add(new WasherSkill() { SkillId = skill.Id, Skill = skill, WasherId = washer.Id, Washer = washer });
                    entities.SaveChanges();
                }
                else
                {
                    return NotFound();
                }
            }
            return Ok();
        }

        [Route("deleteassignment/{skillId}/{washerId}")]
        public IHttpActionResult DeleteAssignment(int skillId, int washerId)
        {
            if (skillId <= 0 || washerId <= 0)
                return BadRequest("Skill/washer id not valid");

            using (SkillDBEntities entities = new SkillDBEntities())
            {
                var skill = entities.Skills.FirstOrDefault(x => x.Id == skillId);
                
                if (skill != null)
                {
                    if (!skill.WasherSkills.Any(x => x.WasherId == washerId))
                    {
                        return BadRequest("Assignment not exists");
                    }

                    var assignmentToDelete = skill.WasherSkills.FirstOrDefault(x => x.WasherId == washerId);

                    entities.WasherSkills.Remove(assignmentToDelete);
                    entities.SaveChanges();
                }
                else
                {
                    return NotFound();
                }
            }
            return Ok();
        }
    }
}

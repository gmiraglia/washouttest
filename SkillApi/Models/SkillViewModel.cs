﻿namespace SkillApi.Models
{
    public class SkillViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}